"use strict";

module.exports = [
	"./source/full-width/industrial/assets/js/scripts/script.js",
	"./source/full-width/industrial/assets/js/scripts/class-modal-window.js",
	"./source/full-width/industrial/assets/js/scripts/class-simple-tabs.js",
	"./source/full-width/industrial/assets/js/scripts/native.js",
	"./source/full-width/industrial/assets/js/scripts/video-player.js",
	"./source/full-width/industrial/assets/js/scripts/img-miniature.js",
	
];