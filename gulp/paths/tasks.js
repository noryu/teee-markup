"use strict";

module.exports = [
	"./gulp/tasks/clean.js",
	"./gulp/tasks/serve.js",

	"./gulp/tasks/full/watch.js",
	"./gulp/tasks/full/serve.js",
	"./gulp/tasks/full/sass.js",
	"./gulp/tasks/full/js.process.js",
	"./gulp/tasks/full/copy.js.js",
	"./gulp/tasks/full/copy.css.js",
	"./gulp/tasks/full/copy.php.js",
	"./gulp/tasks/full/copy.image.js",
	"./gulp/tasks/full/copy.fonts.js",
	"./gulp/tasks/full/html.js",

	"./gulp/tasks/box/watch.js",
	"./gulp/tasks/box/serve.js",
	"./gulp/tasks/box/sass.js",
	"./gulp/tasks/box/js.process.js",
	"./gulp/tasks/box/copy.js.js",
	"./gulp/tasks/box/copy.css.js",
	"./gulp/tasks/box/copy.php.js",
	"./gulp/tasks/box/copy.image.js",
	"./gulp/tasks/box/copy.fonts.js",
	"./gulp/tasks/box/html.js",
];