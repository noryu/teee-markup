"use strict";

module.exports = function() {
	$.gulp.task("box:copy-css", function() {
		return $.gulp.src("./source/box-layout/industrial/assets/css/**/*.*")
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/css"))
	})
};
