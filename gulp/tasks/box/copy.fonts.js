"use strict";

module.exports = function() {
	$.gulp.task("box:copy-fonts", function() {
		return $.gulp.src("./source/box-layout/industrial/assets/fonts/**/*.*", { since: $.gulp.lastRun("box:copy-fonts") })
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/fonts"));
	});
};
