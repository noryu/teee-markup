"use strict";

module.exports = function() {
	$.gulp.task("box:copy-image", function() {
		return $.gulp.src("./source/box-layout/industrial/assets/images/**/*.*", { since: $.gulp.lastRun("box:copy-image") })
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/images"));
	});
};
