"use strict";

module.exports = function() {
	$.gulp.task("box:copy-js", function() {
		return $.gulp.src("./source/box-layout/industrial/assets/js/*.js")
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/js"))
	})
};
