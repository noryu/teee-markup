"use strict";

module.exports = function() {
	$.gulp.task("box:copy-php", function() {
		return $.gulp.src("./source/box-layout/industrial/php/**/*.php")
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/"))
	})
};