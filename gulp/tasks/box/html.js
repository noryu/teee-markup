"use strict";

module.exports = function() {
	$.gulp.task("box:html", function() {
		return $.gulp.src("./source/box-layout/industrial/*.html")
			.pipe($.gp.rigger())
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial"))
			.pipe($.browserSync.stream());
	});
};
