"use strict";

module.exports = function() {
	$.gulp.task("box:js-process", function() {
		return $.gulp.src($.path.app_box)
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.babel())
			.pipe($.gp.concat("script.js"))
			.pipe($.gp.uglify())
			.pipe($.gp.sourcemaps.write("."))
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/js"))
	})
};
