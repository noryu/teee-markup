"use strict";

module.exports = function() {
	$.gulp.task("box:sass", function() {

		var processors = [
			$.autoprefixer({
				browsers: "last 1 version"
			}),
			$.pxtorem({
				rootValue: 16,
				unitPrecision: 5,
				propList: ["font", "font-size", "letter-spacing", "*margin*", "*padding*"],
				selectorBlackList: ["body"],
				replace: true,
				mediaQuery: false,
				minPixelValue: 0
			}),
			$.cssnano({
				preset: ["default", {
					discardComments: { removeAll: true }
				}]
			})
		];

		return $.gulp.src("./source/box-layout/industrial/assets/sass/style.scss")
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.sassGlob())
			.pipe($.gp.sass()).on("error", $.gp.notify.onError({ title: "Style" }))
			.pipe($.postcss(processors))
			.pipe($.gp.sourcemaps.write("."))
			.pipe($.gulp.dest($.config.root + "/box-layout/industrial/assets/css"))
			.pipe($.browserSync.stream());
	})
};
