"use strict";

module.exports = function() {
	$.gulp.task("box:serve", function() {
		$.browserSync.init({
			open: true,
			server: $.config.root + "/box-layout/industrial/"
		});

		$.browserSync.watch([$.config.root + "/box-layout/**/*.*", "!**/*.css"], $.browserSync.reload);
	});
};
