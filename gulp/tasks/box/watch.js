"use strict";

module.exports = function() {
	$.gulp.task("box:watch", function() {
		$.gulp.watch("./source/box-layout/industrial/assets/js/scripts/**/*.js", $.gulp.series("box:js-process"));
		$.gulp.watch("./source/box-layout/industrial/assets/sass/**/*.scss", $.gulp.series("box:sass"));
		$.gulp.watch("./source/box-layout/industrial/assets/js/*.js", $.gulp.series("box:copy-js"));
		$.gulp.watch("./source/box-layout/industrial/assets/css/**/*.*", $.gulp.series("box:copy-css"));
		$.gulp.watch("./source/box-layout/industrial/assets/images/**/*.*", $.gulp.series("box:copy-image"));
		$.gulp.watch("./source/box-layout/industrial/assets/fonts/**/*.*", $.gulp.series("box:copy-fonts"));
		$.gulp.watch("./source/box-layout/industrial/templates/**/*.*", $.gulp.series("box:html"));
		$.gulp.watch("./source/box-layout/industrial/*.html", $.gulp.series("box:html"));
		$.gulp.watch("./source/box-layout/industrial/php/**/*.php", $.gulp.series("box:copy-php"));
	});
};
