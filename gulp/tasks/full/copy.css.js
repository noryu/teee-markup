"use strict";

module.exports = function() {
	$.gulp.task("full:copy-css", function() {
		return $.gulp.src("./source/full-width/industrial/assets/css/**/*.*")
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/assets/css"))
	})
};
