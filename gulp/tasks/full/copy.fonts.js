"use strict";

module.exports = function() {
	$.gulp.task("full:copy-fonts", function() {
		return $.gulp.src("./source/full-width/industrial/assets/fonts/**/*.*", { since: $.gulp.lastRun("full:copy-fonts") })
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/assets/fonts"));
	});
};
