"use strict";

module.exports = function() {
	$.gulp.task("full:copy-image", function() {
		return $.gulp.src("./source/full-width/industrial/assets/images/**/*.*", { since: $.gulp.lastRun("full:copy-image") })
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/assets/images"));
	});
};
