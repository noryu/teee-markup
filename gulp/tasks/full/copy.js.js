"use strict";

module.exports = function() {
	$.gulp.task("full:copy-js", function() {
		return $.gulp.src("./source/full-width/industrial/assets/js/*.js")
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/assets/js"))
	})
};
