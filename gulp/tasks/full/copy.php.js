"use strict";

module.exports = function() {
	$.gulp.task("full:copy-php", function() {
		return $.gulp.src("./source/full-width/industrial/php/**/*.php")
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/"))
	})
};