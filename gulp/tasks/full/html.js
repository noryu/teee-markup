"use strict";

module.exports = function() {
	$.gulp.task("full:html", function() {

		return $.gulp.src("./source/full-width/industrial/*.html")
			.pipe($.gp.rigger())
			.pipe($.gulp.dest($.config.root + "/full-width/industrial"))
			.pipe($.browserSync.stream());
	});
};
