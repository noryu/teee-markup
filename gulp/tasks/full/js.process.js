"use strict";

module.exports = function() {
	$.gulp.task("full:js-process", function() {
		return $.gulp.src($.path.app_full)
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.babel())
			.pipe($.gp.concat("script.js"))
			.pipe($.gp.uglify())
			.pipe($.gp.sourcemaps.write("."))
			.pipe($.gulp.dest($.config.root + "/full-width/industrial/assets/js"))
	})
};
