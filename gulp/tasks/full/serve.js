"use strict";

module.exports = function() {
	$.gulp.task("full:serve", function() {
		$.browserSync.init({
			open: true,
			server: $.config.root + "/full-width/industrial/"
		});

		$.browserSync.watch([$.config.root + "/full-width/**/*.*", "!**/*.css"], $.browserSync.reload);
	});
};
