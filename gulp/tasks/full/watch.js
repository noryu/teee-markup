"use strict";

module.exports = function() {
	$.gulp.task("full:watch", function() {
		$.gulp.watch("./source/full-width/industrial/assets/js/scripts/**/*.js", $.gulp.series("full:js-process"));
		$.gulp.watch("./source/full-width/industrial/assets/sass/**/*.scss", $.gulp.series("full:sass"));
		$.gulp.watch("./source/full-width/industrial/assets/js/*.js", $.gulp.series("full:copy-js"));
		$.gulp.watch("./source/full-width/industrial/assets/css/**/*.*", $.gulp.series("full:copy-css"));
		$.gulp.watch("./source/full-width/industrial/assets/images/**/*.*", $.gulp.series("full:copy-image"));
		$.gulp.watch("./source/full-width/industrial/assets/fonts/**/*.*", $.gulp.series("full:copy-fonts"));
		$.gulp.watch("./source/full-width/industrial/templates/**/*.*", $.gulp.series("full:html"));
		$.gulp.watch("./source/full-width/industrial/*.html", $.gulp.series("full:html"));
		$.gulp.watch("./source/full-width/industrial/php/**/*.php", $.gulp.series("full:copy-php"));
	});
};
