"use strict";

module.exports = function() {
	$.gulp.task("serve", function() {
		$.browserSync.init({
			open: false,
			server: $.config.root + "/full-width/industrial/"
		});

		$.browserSync.watch([$.config.root + "/full-width/**/*.*", "!**/*.css"], $.browserSync.reload);
	});
};
