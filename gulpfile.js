"use strict";

global.$ = {
	package: require("./package.json"),
	config: require("./gulp/config"),
	path: {
		task    : require("./gulp/paths/tasks.js"),
		app_box : require("./gulp/paths/app.box.js"),
		app_full: require("./gulp/paths/app.full.js")
	},
	gulp: require("gulp"),
	del: require("del"),
	sassGlob: require("gulp-sass-glob"),
	postcss: require("gulp-postcss"),
	pxtorem: require("postcss-pxtorem"),
	autoprefixer: require("autoprefixer"),
	cssnano: require("cssnano"),
	browserSync: require("browser-sync").create(),
	gp: require("gulp-load-plugins")(),
	fs: require("fs")
};

$.path.task.forEach(function(taskPath) {
	require(taskPath)();
});

$.gulp.task("default", $.gulp.series(
	"clean",

	$.gulp.parallel(
		"box:sass",
		"box:js-process",
		"box:copy-js",
		"box:copy-image",
		"box:copy-fonts",
		"box:copy-css",
		"box:copy-php",
		"box:html",

		"full:sass",
		"full:js-process",
		"full:copy-js",
		"full:copy-image",
		"full:copy-fonts",
		"full:copy-css",
		"full:copy-php",
		"full:html"
	),

	$.gulp.parallel(
		"box:watch",
		"full:watch",
		"serve"
	)
));

$.gulp.task("box-layout", $.gulp.series(
	"clean",

	$.gulp.parallel(
		"box:sass",
		"box:js-process",
		"box:copy-js",
		"box:copy-image",
		"box:copy-fonts",
		"box:copy-css",
		"box:copy-php",
		"box:html"
	),

	$.gulp.parallel(
		"box:watch",
		"box:serve"
	)
));

$.gulp.task("full-width", $.gulp.series(
	"clean",

	$.gulp.parallel(
		"full:sass",
		"full:js-process",
		"full:copy-js",
		"full:copy-image",
		"full:copy-fonts",
		"full:copy-css",
		"full:copy-php",
		"full:html"
	),

	$.gulp.parallel(
		"full:watch",
		"full:serve"
	)
));